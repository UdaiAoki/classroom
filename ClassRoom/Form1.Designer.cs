﻿namespace ClassRoom
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.absenceButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.randomButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.seats4 = new System.Windows.Forms.TextBox();
            this.seats0 = new System.Windows.Forms.TextBox();
            this.seats1 = new System.Windows.Forms.TextBox();
            this.seats5 = new System.Windows.Forms.TextBox();
            this.seats6 = new System.Windows.Forms.TextBox();
            this.seats7 = new System.Windows.Forms.TextBox();
            this.seats9 = new System.Windows.Forms.TextBox();
            this.seats10 = new System.Windows.Forms.TextBox();
            this.seats11 = new System.Windows.Forms.TextBox();
            this.seats12 = new System.Windows.Forms.TextBox();
            this.nowTurnBox = new System.Windows.Forms.TextBox();
            this.nowNameBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.seats2 = new System.Windows.Forms.TextBox();
            this.seats3 = new System.Windows.Forms.TextBox();
            this.seats8 = new System.Windows.Forms.TextBox();
            this.readButton = new System.Windows.Forms.Button();
            this.attendButton = new System.Windows.Forms.Button();
            this.showButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.reduceButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(24, 285);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "名前";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(24, 314);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "回数";
            // 
            // absenceButton
            // 
            this.absenceButton.Enabled = false;
            this.absenceButton.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.absenceButton.Location = new System.Drawing.Point(569, 295);
            this.absenceButton.Name = "absenceButton";
            this.absenceButton.Size = new System.Drawing.Size(67, 40);
            this.absenceButton.TabIndex = 4;
            this.absenceButton.Text = "欠席";
            this.absenceButton.UseVisualStyleBackColor = true;
            this.absenceButton.Click += new System.EventHandler(this.AbsenceButtonClicked);
            // 
            // nextButton
            // 
            this.nextButton.Enabled = false;
            this.nextButton.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.nextButton.Location = new System.Drawing.Point(225, 293);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(67, 40);
            this.nextButton.TabIndex = 5;
            this.nextButton.Text = "次へ";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.NextButtonClicked);
            // 
            // randomButton
            // 
            this.randomButton.Enabled = false;
            this.randomButton.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.randomButton.Location = new System.Drawing.Point(477, 295);
            this.randomButton.Name = "randomButton";
            this.randomButton.Size = new System.Drawing.Size(74, 40);
            this.randomButton.TabIndex = 6;
            this.randomButton.Text = "ランダム";
            this.randomButton.UseVisualStyleBackColor = true;
            this.randomButton.Click += new System.EventHandler(this.RandomButtonClicked);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1.Location = new System.Drawing.Point(12, 21);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(43, 26);
            this.textBox1.TabIndex = 7;
            this.textBox1.Text = "教員";
            // 
            // seats4
            // 
            this.seats4.Enabled = false;
            this.seats4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.seats4.Location = new System.Drawing.Point(12, 67);
            this.seats4.Name = "seats4";
            this.seats4.ReadOnly = true;
            this.seats4.Size = new System.Drawing.Size(59, 23);
            this.seats4.TabIndex = 8;
            this.seats4.Click += new System.EventHandler(this.SeatsBox4Clicked);
            // 
            // seats0
            // 
            this.seats0.BackColor = System.Drawing.SystemColors.Control;
            this.seats0.Enabled = false;
            this.seats0.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.seats0.Location = new System.Drawing.Point(118, 24);
            this.seats0.Name = "seats0";
            this.seats0.ReadOnly = true;
            this.seats0.Size = new System.Drawing.Size(56, 23);
            this.seats0.TabIndex = 9;
            this.seats0.Click += new System.EventHandler(this.SeatsBoxClicked);
            // 
            // seats1
            // 
            this.seats1.Enabled = false;
            this.seats1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.seats1.Location = new System.Drawing.Point(180, 24);
            this.seats1.Name = "seats1";
            this.seats1.ReadOnly = true;
            this.seats1.Size = new System.Drawing.Size(64, 23);
            this.seats1.TabIndex = 10;
            this.seats1.Click += new System.EventHandler(this.SeatsBox1Clicked);
            // 
            // seats5
            // 
            this.seats5.Enabled = false;
            this.seats5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.seats5.Location = new System.Drawing.Point(118, 67);
            this.seats5.Name = "seats5";
            this.seats5.ReadOnly = true;
            this.seats5.Size = new System.Drawing.Size(56, 23);
            this.seats5.TabIndex = 13;
            this.seats5.Click += new System.EventHandler(this.SeatsBox5Clicked);
            // 
            // seats6
            // 
            this.seats6.Enabled = false;
            this.seats6.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.seats6.Location = new System.Drawing.Point(180, 67);
            this.seats6.Name = "seats6";
            this.seats6.ReadOnly = true;
            this.seats6.Size = new System.Drawing.Size(64, 23);
            this.seats6.TabIndex = 14;
            this.seats6.Click += new System.EventHandler(this.SeatsBox6Clicked);
            // 
            // seats7
            // 
            this.seats7.Enabled = false;
            this.seats7.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.seats7.Location = new System.Drawing.Point(250, 67);
            this.seats7.Name = "seats7";
            this.seats7.ReadOnly = true;
            this.seats7.Size = new System.Drawing.Size(56, 23);
            this.seats7.TabIndex = 15;
            this.seats7.Click += new System.EventHandler(this.SeatsBox7Clicked);
            // 
            // seats9
            // 
            this.seats9.Enabled = false;
            this.seats9.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.seats9.Location = new System.Drawing.Point(118, 110);
            this.seats9.Name = "seats9";
            this.seats9.ReadOnly = true;
            this.seats9.Size = new System.Drawing.Size(56, 23);
            this.seats9.TabIndex = 17;
            this.seats9.Click += new System.EventHandler(this.SeatsBox9Clicked);
            // 
            // seats10
            // 
            this.seats10.Enabled = false;
            this.seats10.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.seats10.Location = new System.Drawing.Point(180, 110);
            this.seats10.Name = "seats10";
            this.seats10.ReadOnly = true;
            this.seats10.Size = new System.Drawing.Size(64, 23);
            this.seats10.TabIndex = 18;
            this.seats10.Click += new System.EventHandler(this.SeatsBox10Clicked);
            // 
            // seats11
            // 
            this.seats11.Enabled = false;
            this.seats11.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.seats11.Location = new System.Drawing.Point(118, 149);
            this.seats11.Name = "seats11";
            this.seats11.ReadOnly = true;
            this.seats11.Size = new System.Drawing.Size(56, 23);
            this.seats11.TabIndex = 19;
            this.seats11.Click += new System.EventHandler(this.SeatsBox11Clicked);
            // 
            // seats12
            // 
            this.seats12.Enabled = false;
            this.seats12.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.seats12.Location = new System.Drawing.Point(180, 149);
            this.seats12.Name = "seats12";
            this.seats12.ReadOnly = true;
            this.seats12.Size = new System.Drawing.Size(64, 23);
            this.seats12.TabIndex = 20;
            this.seats12.Click += new System.EventHandler(this.SeatsBox12Clicked);
            // 
            // nowTurnBox
            // 
            this.nowTurnBox.Enabled = false;
            this.nowTurnBox.Location = new System.Drawing.Point(77, 314);
            this.nowTurnBox.Name = "nowTurnBox";
            this.nowTurnBox.ReadOnly = true;
            this.nowTurnBox.Size = new System.Drawing.Size(76, 19);
            this.nowTurnBox.TabIndex = 21;
            // 
            // nowNameBox
            // 
            this.nowNameBox.Enabled = false;
            this.nowNameBox.Location = new System.Drawing.Point(77, 285);
            this.nowNameBox.Name = "nowNameBox";
            this.nowNameBox.ReadOnly = true;
            this.nowNameBox.Size = new System.Drawing.Size(76, 19);
            this.nowNameBox.TabIndex = 24;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(24, 252);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 19);
            this.label3.TabIndex = 25;
            this.label3.Text = "現在";
            // 
            // seats2
            // 
            this.seats2.Enabled = false;
            this.seats2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.seats2.Location = new System.Drawing.Point(250, 24);
            this.seats2.Name = "seats2";
            this.seats2.ReadOnly = true;
            this.seats2.Size = new System.Drawing.Size(56, 23);
            this.seats2.TabIndex = 26;
            this.seats2.Click += new System.EventHandler(this.SeatsBox2Clicked);
            // 
            // seats3
            // 
            this.seats3.BackColor = System.Drawing.SystemColors.Control;
            this.seats3.Enabled = false;
            this.seats3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.seats3.Location = new System.Drawing.Point(312, 24);
            this.seats3.Name = "seats3";
            this.seats3.ReadOnly = true;
            this.seats3.Size = new System.Drawing.Size(61, 23);
            this.seats3.TabIndex = 27;
            this.seats3.Click += new System.EventHandler(this.SeatsBox3Clicked);
            // 
            // seats8
            // 
            this.seats8.Enabled = false;
            this.seats8.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.seats8.Location = new System.Drawing.Point(312, 67);
            this.seats8.Name = "seats8";
            this.seats8.ReadOnly = true;
            this.seats8.Size = new System.Drawing.Size(61, 23);
            this.seats8.TabIndex = 28;
            this.seats8.Click += new System.EventHandler(this.SeatsBox8Clicked);
            // 
            // readButton
            // 
            this.readButton.Location = new System.Drawing.Point(427, 24);
            this.readButton.Name = "readButton";
            this.readButton.Size = new System.Drawing.Size(64, 35);
            this.readButton.TabIndex = 29;
            this.readButton.Text = "データを読み込む";
            this.readButton.UseVisualStyleBackColor = true;
            this.readButton.Click += new System.EventHandler(this.ReadButtonClicked);
            // 
            // attendButton
            // 
            this.attendButton.Enabled = false;
            this.attendButton.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.attendButton.Location = new System.Drawing.Point(569, 244);
            this.attendButton.Name = "attendButton";
            this.attendButton.Size = new System.Drawing.Size(64, 40);
            this.attendButton.TabIndex = 30;
            this.attendButton.Text = "出席";
            this.attendButton.UseVisualStyleBackColor = true;
            this.attendButton.Click += new System.EventHandler(this.AttendButtonClicked);
            // 
            // showButton
            // 
            this.showButton.Enabled = false;
            this.showButton.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.showButton.Location = new System.Drawing.Point(477, 247);
            this.showButton.Name = "showButton";
            this.showButton.Size = new System.Drawing.Size(74, 37);
            this.showButton.TabIndex = 31;
            this.showButton.Text = "一覧";
            this.showButton.UseVisualStyleBackColor = true;
            this.showButton.Click += new System.EventHandler(this.ShowButtonClicked);
            // 
            // addButton
            // 
            this.addButton.Enabled = false;
            this.addButton.Location = new System.Drawing.Point(159, 285);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(25, 19);
            this.addButton.TabIndex = 32;
            this.addButton.Text = "▲";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.AddButtonClicked);
            // 
            // reduceButton
            // 
            this.reduceButton.Enabled = false;
            this.reduceButton.Location = new System.Drawing.Point(159, 314);
            this.reduceButton.Name = "reduceButton";
            this.reduceButton.Size = new System.Drawing.Size(25, 19);
            this.reduceButton.TabIndex = 33;
            this.reduceButton.Text = "▼";
            this.reduceButton.UseVisualStyleBackColor = true;
            this.reduceButton.Click += new System.EventHandler(this.ReduceButtonClicked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 347);
            this.Controls.Add(this.reduceButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.showButton);
            this.Controls.Add(this.attendButton);
            this.Controls.Add(this.readButton);
            this.Controls.Add(this.seats8);
            this.Controls.Add(this.seats3);
            this.Controls.Add(this.seats2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nowNameBox);
            this.Controls.Add(this.nowTurnBox);
            this.Controls.Add(this.seats12);
            this.Controls.Add(this.seats11);
            this.Controls.Add(this.seats10);
            this.Controls.Add(this.seats9);
            this.Controls.Add(this.seats7);
            this.Controls.Add(this.seats6);
            this.Controls.Add(this.seats5);
            this.Controls.Add(this.seats1);
            this.Controls.Add(this.seats0);
            this.Controls.Add(this.seats4);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.randomButton);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.absenceButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button absenceButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button randomButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox nowTurnBox;
        private System.Windows.Forms.TextBox nowNameBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button readButton;
        private System.Windows.Forms.Button attendButton;
        private System.Windows.Forms.Button showButton;
        public System.Windows.Forms.TextBox seats4;
        public System.Windows.Forms.TextBox seats0;
        public System.Windows.Forms.TextBox seats1;
        public System.Windows.Forms.TextBox seats5;
        public System.Windows.Forms.TextBox seats6;
        public System.Windows.Forms.TextBox seats7;
        public System.Windows.Forms.TextBox seats9;
        public System.Windows.Forms.TextBox seats10;
        public System.Windows.Forms.TextBox seats11;
        public System.Windows.Forms.TextBox seats12;
        public System.Windows.Forms.TextBox seats2;
        public System.Windows.Forms.TextBox seats3;
        public System.Windows.Forms.TextBox seats8;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button reduceButton;
    }
}

