﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClassRoom
{

    public partial class Form1 : Form
    {
        List<TextBox> seats;
        List<string> seatsTurn;
        Table table;

        public Form1()
        {
            InitializeComponent();
            this.table = new Table();
            table.Hide();
            seatsTurn = new List<string>();
            seats = new List<TextBox>();
            
            seats.Add(seats0);
            seats.Add(seats1);
            seats.Add(seats2);
            seats.Add(seats3);
            seats.Add(seats4);
            seats.Add(seats5);
            seats.Add(seats6);
            seats.Add(seats7);
            seats.Add(seats8);
            seats.Add(seats9);
            seats.Add(seats10);
            seats.Add(seats11);
            seats.Add(seats12);
        }

        //data.txt(CSV形式)読み込みボタン
        private void ReadButtonClicked(object sender, EventArgs e)
        {
            using (System.IO.StreamReader file =
                new System.IO.StreamReader(@"..\..\data.txt"))
            {

                this.readButton.Enabled = false;
                for(int i = 0; !file.EndOfStream; i++)
                {
                    string line = file.ReadLine();
                    string[] data = line.Split(',');
                    if (!data[0].Equals("") || !data[1].Equals(""))
                    {
                        seats[i].Text = data[0];
                        seatsTurn.Add(data[1]);
                        this.table.turnList.Items.Add(seatsTurn[i]);
                        this.table.nameList.Items.Add(seats[i].Text);
                    }
                    else
                    {
                        seatsTurn.Add(data[1]);
                        this.seats[i].BackColor = Color.LightPink;
                        this.table.turnList.Items.Add(seatsTurn[i]);
                        this.table.nameList.Items.Add(seats[i].Text);
                    }
                }
                ChangeEnabled();
            }
        }

        private void SeatsBoxClicked(object sender, EventArgs e)
        {
            if (!this.seats0.Text.Equals(""))
            {
                this.nowNameBox.Text = this.seats0.Text;
                this.nowTurnBox.Text = seatsTurn[0];
                Attendance();
            }
        }

        private void SeatsBox1Clicked(object sender, EventArgs e)
        {
            if (!this.seats1.Text.Equals(""))
            {
                this.nowNameBox.Text = this.seats1.Text;
                this.nowTurnBox.Text = seatsTurn[1];
                Attendance();
            }
        }

        private void SeatsBox2Clicked(object sender, EventArgs e)
        {
            if (!this.seats2.Text.Equals(""))
            {
                this.nowNameBox.Text = this.seats2.Text;
                this.nowTurnBox.Text = seatsTurn[2];
                Attendance();
            }
        }
        
        private void SeatsBox3Clicked(object sender, EventArgs e)
        {
            if (!this.seats3.Text.Equals(""))
            {
                this.nowNameBox.Text = this.seats3.Text;
                this.nowTurnBox.Text = seatsTurn[3];
                Attendance();
            }
        }

        private void SeatsBox4Clicked(object sender, EventArgs e)
        {
            if (!this.seats4.Text.Equals(""))
            {
                this.nowNameBox.Text = this.seats4.Text;
                this.nowTurnBox.Text = seatsTurn[4];
                Attendance();
            }
        }

        private void SeatsBox5Clicked(object sender, EventArgs e)
        {
            if (!this.seats5.Text.Equals(""))
            {
                this.nowNameBox.Text = this.seats5.Text;
                this.nowTurnBox.Text = seatsTurn[5];
                Attendance();
            }
        }

        private void SeatsBox6Clicked(object sender, EventArgs e)
        {
            if (!this.seats6.Text.Equals(""))
            {
                this.nowNameBox.Text = this.seats6.Text;
                this.nowTurnBox.Text = seatsTurn[6];
                Attendance();
            }
        }

        private void SeatsBox7Clicked(object sender, EventArgs e)
        {
            if (!this.seats7.Text.Equals(""))
            {
                this.nowNameBox.Text = this.seats7.Text;
                this.nowTurnBox.Text = seatsTurn[7];
                Attendance();
            }
        }

        private void SeatsBox8Clicked(object sender, EventArgs e)
        {
            if (!this.seats8.Text.Equals(""))
            {
                this.nowNameBox.Text = this.seats8.Text;
                this.nowTurnBox.Text = seatsTurn[8];
                Attendance();
            }
        }

        private void SeatsBox9Clicked(object sender, EventArgs e)
        {
            if (!this.seats9.Text.Equals(""))
            {
                this.nowNameBox.Text = this.seats9.Text;
                this.nowTurnBox.Text = seatsTurn[9];
                Attendance();
            }
        }

        private void SeatsBox10Clicked(object sender, EventArgs e)
        {
            if (!this.seats10.Text.Equals(""))
            {
                this.nowNameBox.Text = this.seats10.Text;
                this.nowTurnBox.Text = seatsTurn[10];
                Attendance();
            }
        }

        private void SeatsBox11Clicked(object sender, EventArgs e)
        {
            if (!this.seats11.Text.Equals(""))
            {
                this.nowNameBox.Text = this.seats11.Text;
                this.nowTurnBox.Text = seatsTurn[11];
                Attendance();
            }
        }

        private void SeatsBox12Clicked(object sender, EventArgs e)
        {
            if (!this.seats12.Text .Equals(""))
            {
                this.nowNameBox.Text = this.seats12.Text;
                this.nowTurnBox.Text = seatsTurn[12];
                Attendance();
            }
        }

        private void NextButtonClicked(object sender, EventArgs e)
        {
            for (int i = 0; i < seats.Count; i++)
            {
                if (this.nowNameBox.Text.Equals(seats[i].Text) && this.seats[i].BackColor != Color.LightPink)
                {
                    int turn = int.Parse(this.nowTurnBox.Text);
                    turn++;
                    string addTurn = turn.ToString();
                    seatsTurn[i] = addTurn;
                    int next = FindNextName(i);
                    this.nowNameBox.Text = this.seats[next].Text;
                    this.nowTurnBox.Text = seatsTurn[next];
                    break;
                }
            }
        }
        //次に表示させるTextを探すメソッド
        private int FindNextName(int i)
        {
            int j;
            for(j = i+1; j <= seats.Count;)
            {
                if (j == seats.Count)
                {
                    j = 0;
                    if (seats[j].BackColor == Color.LightPink || seats[j].Text.Equals(""))
                    {
                        j++;
                        continue;
                    }
                    break;
                }
                else if (seats[j].Text.Equals("") || this.seats[j].BackColor == Color.LightPink)
                {
                    j++;
                    continue;
                }
                break;
            }
            return j;
        }
        //欠席ボタン
        private void AbsenceButtonClicked(object sender, EventArgs e)
        {
            for (int i = 0; i < seats.Count; i++)
            {
                if (this.nowNameBox.Text.Equals(seats[i].Text))
                {
                    this.seats[i].BackColor = Color.LightPink;
                }
            }
        }
        //ランダムボタン
        private void RandomButtonClicked(object sender, EventArgs e)
        {
            Random r = new Random();
            while (true)
            {
                int num = r.Next(seats.Count);
                if (seats[num].BackColor == Color.LightPink)
                {
                    continue;
                }
                this.nowNameBox.Text = seats[num].Text;
                this.nowTurnBox.Text = seatsTurn[num];
                break;
            }
            Attendance();
        }
        //出席ボタン、TextBoxのBackColorをデフォルトにする
        private void AttendButtonClicked(object sender, EventArgs e)
        {
            for(int i = 0; i < seats.Count; i++)
            {
                if (nowNameBox.Text.Equals(seats[i].Text) && seats[i].BackColor == Color.LightPink)
                {
                    seats[i].BackColor = SystemColors.Control;
                    break;
                }
            }
        }
        //一覧ボタン(サブフォームを開く)
        private void ShowButtonClicked(object sender, EventArgs e)
        {
            if(this.readButton.Enabled == false)
            {
                this.table.Show();
            }
        }
        //TextBox、ButtonのEnabledをtrueにするメソッド
        private void ChangeEnabled()
        {
            for(int i = 0; i < seats.Count; i++)
            {
                seats[i].Enabled = true;
            }
            this.randomButton.Enabled = true;
            this.showButton.Enabled = true;
            this.nextButton.Enabled = true;
            this.nowNameBox.Enabled = true;
            this.nowTurnBox.Enabled = true;
        }
        private void Attendance()
        {
            this.attendButton.Enabled = true;
            this.absenceButton.Enabled = true;
            this.addButton.Enabled = true;
            this.reduceButton.Enabled = true;
        }

        private void AddButtonClicked(object sender, EventArgs e)
        {
            for(int i = 0; i < seats.Count; i++)
            {
                if (nowNameBox.Text.Equals(seats[i].Text))
                {
                    int add = int.Parse(nowTurnBox.Text);
                    add++;
                    seatsTurn[i] = add.ToString();
                    this.nowTurnBox.Text = seatsTurn[i];
                }
            }
        }

        private void ReduceButtonClicked(object sender, EventArgs e)
        {
            for (int i = 0; i < seats.Count; i++)
            {
                if (nowNameBox.Text.Equals(seats[i].Text))
                {
                    int reduce = int.Parse(nowTurnBox.Text);
                    reduce--;
                    seatsTurn[i] = reduce.ToString();
                    this.nowTurnBox.Text = seatsTurn[i];
                }
            }
        }
    }
}
