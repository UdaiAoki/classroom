﻿namespace ClassRoom
{
    partial class Table
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameList = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.turnList = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.hideButtun = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // nameList
            // 
            this.nameList.FormattingEnabled = true;
            this.nameList.ItemHeight = 12;
            this.nameList.Location = new System.Drawing.Point(23, 62);
            this.nameList.Name = "nameList";
            this.nameList.Size = new System.Drawing.Size(146, 184);
            this.nameList.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(21, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "名前";
            // 
            // turnList
            // 
            this.turnList.FormattingEnabled = true;
            this.turnList.ItemHeight = 12;
            this.turnList.Location = new System.Drawing.Point(166, 62);
            this.turnList.Name = "turnList";
            this.turnList.Size = new System.Drawing.Size(146, 184);
            this.turnList.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(162, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "回数";
            // 
            // hideButtun
            // 
            this.hideButtun.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.hideButtun.Location = new System.Drawing.Point(409, 197);
            this.hideButtun.Name = "hideButtun";
            this.hideButtun.Size = new System.Drawing.Size(84, 49);
            this.hideButtun.TabIndex = 4;
            this.hideButtun.Text = "閉じる";
            this.hideButtun.UseVisualStyleBackColor = true;
            this.hideButtun.Click += new System.EventHandler(this.hideButtunClicked);
            // 
            // updateButton
            // 
            this.updateButton.Font = new System.Drawing.Font("MS UI Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.updateButton.Location = new System.Drawing.Point(409, 130);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(84, 48);
            this.updateButton.TabIndex = 5;
            this.updateButton.Text = "更新";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.UpdateButtonClicked);
            // 
            // Table
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 289);
            this.ControlBox = false;
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.hideButtun);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.turnList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nameList);
            this.Name = "Table";
            this.Text = "Table";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ListBox nameList;
        public System.Windows.Forms.ListBox turnList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button hideButtun;
        private System.Windows.Forms.Button updateButton;
    }
}